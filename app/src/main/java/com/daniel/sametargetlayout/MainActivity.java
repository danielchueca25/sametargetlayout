package com.daniel.sametargetlayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static String PRODUCT_TITLE = "com.example.edt16b.PRODUCT_TITLE";
    public static String PRODUCT_SUBTITLE = "com.example.edt16b.PRODUCT_SUBTITLE";
    public static String PRODUCT_IMAGE = "com.example.edt16b.PRODUCT_IMAGE";
    public static String PRODUCT_TYPE = "com.example.edt16b.PRODUCT_TYPE";
    public static String PRODUCT_NUM = "com.example.edt16b.PRODUCT_NUM";
    public static String PRODUCT_DESC = "com.example.edt16b.PRODUCT_DESC";
    public static String PRODUCT_HEIGHT = "com.example.edt16b.PRODUCT_HEIGHT";

    public static String PRODUCT_WEIGHT = "com.example.edt16b.PRODUCT_IWEIGHT";


    private CardView cardview11;
    private CardView cardview21;
    private CardView cardview31;
    private CardView cardview41;
    private CardView cardview51;
    private CardView cardview61;
    private CardView cardview71;
    private CardView cardview81;
    private CardView cardview91;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Hook
        cardview11 = (CardView) findViewById(R.id.cardView11);
        cardview21 = (CardView) findViewById(R.id.cardView21);
        cardview31 = (CardView) findViewById(R.id.cardView31);
        cardview41 = (CardView) findViewById(R.id.cardView41);
        cardview51 = (CardView) findViewById(R.id.cardView51);
        cardview61 = (CardView) findViewById(R.id.cardView61);
        cardview71 = (CardView) findViewById(R.id.cardView71);
        cardview81 = (CardView) findViewById(R.id.cardView81);
        cardview91 = (CardView) findViewById(R.id.cardView91);

        cardview11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Bulbasaur";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.bulbasaur;
                String productDesc= "Bulbasaur es un Pokémon de tipo planta/veneno introducido en la primera generación. Es uno de los Pokémon iniciales que pueden elegir los entrenadores que empiezan su aventura en la región de Kanto, junto a Squirtle y Charmander, en las ediciones Pokémon Rojo, Pokémon Verde y Pokémon Azul y Pokémon Rojo Fuego y Pokémon Verde Hoja. Se destaca por ser el primer Pokémon de la Pokédex Nacional.\n" +
                        "\n\n" +
                        "- Su nombre proviene de las palabra inglesa bulb (bulbo) y de la palabra griega saur (reptil o lagarto).\n\n" +
                        "- Su nombre japonés, Fushigidane, proviene de 不思議種 fushigidane (semilla extraña) y de un juego de palabras con 不思議だね fushigi da ne (es extraño, ¿verdad?).\n\n" +
                        "- Su nombre francés, Bulbizarre, procede de las palabras francesas bulbe (bulbo) y bizarre (extraño).\n\n" +
                        "- Su nombre alemán, Bisasam, procede de las palabras alemanas Bi (dos), Saurier (dinosaurio) y Samen (semilla).\n\n" +
                        "- Su nombre coreano, 이상해씨 (Isanghaessi), procede de 이상해 isanghada (extraño) y 씨 ssi (semilla).\n\n";
                String productHeight = "Height : 0.7m";
                String productWeight = "Weight : 6.9kg";
                String productType = "Grass/Poison";
                String productNum = "001";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });
        cardview21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Ivysaur";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.ivysaur;
                String productDesc= "Ivysaur es un Pokémon de tipo planta/veneno introducido en la primera generación. Es la evolución de Bulbasaur, uno de los Pokémon iniciales de Kanto.\n\n" +
                        "- Su nombre proviene de la palabra inglesa ivy (hiedra) y del griego saur (reptil o lagarto).\n\n" +
                        "- Su nombre japonés, Fushigisou, procede de 不思議 草 fushigi sou (hierba extraña). Además, es un juego de palabras al igual que Bulbasaur. En este caso significa parece extraño.\n\n" +
                        "- Su nombre francés, Herbizarre, proviene de las palabras francesas herbe (hierba) y bizarre (raro o extraño).\n\n" +
                        "- Su nombre alemán, Bisaknosp, proviene de Bi (dos), saurier (dinosaurio) y knospe (brote).\n" ;
                String productHeight = "Height : 1.0m";
                String productWeight = "Weight : 13.0kg";
                String productType = "Grass/Poison";
                String productNum = "002";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });
        cardview31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Venusaur";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.venusaur;
                String productDesc= "Venusaur es un Pokémon de tipo planta/veneno introducido en la primera generación. Es la evolución de Ivysaur.\n" +
                        "\n\n" +
                        "- Su nombre proviene de venus atrapamoscas y de la palabra griega saur (reptil o lagarto).\n\n" +
                        "- Su nombre japonés, Fushigibana, proviene de 不思議花 fushigi na hana (flor extraña). Mantiene como Ivysaur y Bulbasaur la referencia a lo extraños que pueden resultar.\n\n" +
                        "- Su nombre francés, Florizarre, proviene de las palabras francesas fleur (flor) y bizarre (extraño).\n";

                String productHeight = "Height : 2.0m";
                String productWeight = "Weight : 100.0kg";
                String productType = "Grass/Poison";
                String productNum = "003";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });
        cardview41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Charmander";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.charmander;
                String productDesc= "Charmander es un Pokémon de tipo fuego introducido en la primera generación. Es uno de los Pokémon " +
                        "iniciales que pueden elegir los entrenadores que empiezan su aventura en la región Kanto, junto a Bulbasaur " +
                        "y Squirtle, en las ediciones Pokémon Rojo, Pokémon Verde y Pokémon Azul y Pokémon Rojo Fuego y Pokémon Verde Hoja.\n" +
                        "\n\n" +
                        "- Su nombre proviene de las palabras inglesas charcoal (carbón) y salamander (salamandra).\n\n" +
                        "- Su nombre japonés, Hitokage, procede de 火 hi (fuego) y 蜥蜴 tokage (lagarto).\n\n" +
                        "- Su nombre francés, Salamèche, procede de las palabras francesas salamandre (salamandra) y mèche (mecha).\n\n" +
                        "- Su nombre alemán, Glumanda, procede de glut (brillar) y salamander (salamandra)\n";

                String productHeight = "Height : 0.6m";
                String productWeight = "Weight : 8.5kg";
                String productType = "Fire";
                String productNum = "004";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });cardview51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Charmeleon";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.charmeleon;
                String productDesc= "Charmeleon es un Pokémon de tipo fuego introducido en la primera generación. Es la evolución de Charmander, " +
                        "uno de los Pokémon iniciales de los entrenadores que comienzan su aventura en la región de Kanto.\n\n\n" +
                        "- Su nombre es una contracción de las palabras inglesas char (carbonizar, quemar) y chameleon (camaleón).\n" +
                        "\n" +
                        "- Su nombre japonés, lizardo, es simplemente la escritura en katakana de la palabra lizard (lagarto en inglés).\n" +
                        "\n" +
                        "- Su nombre francés, reptincel, proviene de las palabras reptil (reptil) y étincelle (chispa).\n";

                String productHeight = "Height : 1.1m";
                String productWeight = "Weight : 19.0kg";
                String productType = "Fire";
                String productNum = "005";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });cardview61.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Charizard";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.charizard;
                String productDesc= "Charizard es un Pokémon de tipo fuego/volador, introducido en la primera generación. Es la evolución de Charmeleon.\n" +
                        "\n" +
                        "\n" +
                        "- Su nombre proviene de las palabras inglesas char (carbonizar, incinerar) y lizard (lagarto).\n\n" +
                        "- Su nombre en japonés, Lizardon, es una combinación de lizard (lagarto en inglés) y don, que es un sufijo que se utiliza comúnmente en dinosaurios y significa diente.\n\n" +
                        "- Su nombre francés, Dracaufeu, proviene de las palabras draco (dragón) y feu (fuego).\n";

                String productHeight = "Height : 1.7m";
                String productWeight = "Weight : 90.5kg";
                String productType = "Fire/Flying";
                String productNum = "006";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });cardview71.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Squirtle";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.squirtle;
                String productDesc= "Squirtle es un Pokémon de tipo agua introducido en la primera generación. Es uno de los Pokémon iniciales " +
                        "en la región Kanto, junto a Bulbasaur y Charmander, en las ediciones Pokémon Rojo, Pokémon Verde y Pokémon Azul y Pokémon Rojo Fuego y Pokémon Verde Hoja.\n" +
                        "\n\n" +
                        "- Su nombre proviene de las palabras en inglés squirt (disparar un chorro de agua) y turtle (tortuga).\n" +
                        "- Su nombre en japonés, Zenigame, es simplemente la palabra japonesa para tortuga de estanque, 銭亀.\n" +
                        "- Su nombre francés, Carapuce, proviene de las palabras carapace (caparazón) y puce (Pulga, en referencia a su tamaño). Puce también puede ser un término cariñoso, por su apariencia tierna y dulce.\n" +
                        "- Su nombre alemán, Schiggy, proviene de la palabra schildkröte (tortuga).\n";

                String productHeight = "Height : 0.5m";
                String productWeight = "Weight : 9.0kg";
                String productType = "Water";
                String productNum = "007";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });cardview81.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Wartortle";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.wartortle;
                String productDesc= "Wartortle es un Pokémon de tipo agua introducido en la primera generación. Es la evolución de Squirtle, uno de los Pokémon iniciales de Kanto.\n" +
                        "\n\n" +
                        "- Su nombre proviene de las palabras en inglés war (guerra) y turtle (tortuga).\n\n" +
                        "- Su nombre japonés, Kameil, proviene de kame (tortuga) y probablemente de tail (cola).\n\n" +
                        "- Su nombre francés, Carabaffe, viene de carapace (caparazón) y Baffe (bofetada).\n";

                String productHeight = "Height : 1.0m";
                String productWeight = "Weight : 22.5kg";
                String productType = "Water";
                String productNum = "008";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });
        cardview91.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle="Blastoise";
                String productSubTitle="Pokemon Details";
                int productImage = R.drawable.blastoise;
                String productDesc= "Blastoise es un Pokémon de tipo agua introducido en la primera generación. Es la evolución de Wartortle.\n" +
                        "\n\n" +
                        "- Su nombre proviene de las palabras inglesas blast (explosión o ráfaga) y tortoise (tortuga terrestre).\n" +
                        "- Su nombre japonés, Kamex, proviene de 亀 kame (tortuga) y de max (máximo).\n" +
                        "- Su nombre francés, Tortank, proviene de las palabras tortue (tortuga) y tank (tanque).\n";

                String productHeight = "Height : 1.6m";
                String productWeight = "Weight : 85.5kg";
                String productType = "Water";
                String productNum = "009";
                Intent intent = new Intent(MainActivity.this, CardViewActivity.class);
                intent.putExtra(PRODUCT_TITLE,productTitle);
                intent.putExtra(PRODUCT_SUBTITLE,productSubTitle);
                intent.putExtra(PRODUCT_IMAGE, productImage);

                intent.putExtra(PRODUCT_DESC,productDesc);
                intent.putExtra(PRODUCT_HEIGHT,productHeight);
                intent.putExtra(PRODUCT_WEIGHT,productWeight);
                intent.putExtra(PRODUCT_TYPE,productType);
                intent.putExtra(PRODUCT_NUM,productNum);
                startActivity(intent);
            }
        });




    }
}