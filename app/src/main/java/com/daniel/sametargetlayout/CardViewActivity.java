package com.daniel.sametargetlayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class CardViewActivity extends AppCompatActivity {
    private TextView textTitle;
    private TextView textSub;
    private ImageView productImage;
    private ImageView logo;
    private ImageView imageBig;
    private TextView descript;
    private TextView name;
    private TextView type;
    private TextView pokedexNum;
    private TextView height;
    private TextView weight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view);

        //Hook
        textTitle = (TextView) findViewById(R.id.textTitle);
        textSub = (TextView) findViewById(R.id.textSub);
        productImage = (ImageView) findViewById(R.id.productImage);
        logo = (ImageView) findViewById(R.id.logo);
        imageBig = (ImageView) findViewById(R.id.imageBig);
        descript = (TextView) findViewById(R.id.descript);
        name = (TextView) findViewById(R.id.name);
        type = (TextView) findViewById(R.id.type);
        pokedexNum = (TextView) findViewById(R.id.pokedexNum);
        height = (TextView) findViewById(R.id.height);
        weight = (TextView) findViewById(R.id.weight);


        Intent i =getIntent();
        String sProductTitle = i.getStringExtra(MainActivity.PRODUCT_TITLE);
        String sProductName = i.getStringExtra(MainActivity.PRODUCT_TITLE);
        String sProductSubTitle = i.getStringExtra(MainActivity.PRODUCT_SUBTITLE);
        int iProductImage =i.getIntExtra(MainActivity.PRODUCT_IMAGE, 0);
        String sProductDesc = i.getStringExtra(MainActivity.PRODUCT_DESC);
        String sProductType = i.getStringExtra(MainActivity.PRODUCT_TYPE);
        String sProductNum = i.getStringExtra(MainActivity.PRODUCT_NUM);
        String sProductHeight = i.getStringExtra(MainActivity.PRODUCT_HEIGHT);
        String sProductWeight = i.getStringExtra(MainActivity.PRODUCT_WEIGHT);

        textTitle.setText(sProductTitle);
        textSub.setText(sProductSubTitle);
        name.setText(sProductName);
        logo.setImageResource(iProductImage);
        productImage.setImageResource(iProductImage);
        imageBig.setImageResource(iProductImage);
        descript.setText(sProductDesc);
        height.setText(sProductHeight);
        weight.setText(sProductWeight);
        type.setText(sProductType);
        pokedexNum.setText(sProductNum);
    }
}